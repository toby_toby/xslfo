<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    exclude-result-prefixes="fo">

  
  <xsl:template match="root">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
             font-family="Helvetica"
             line-height="1.5">
      <fo:layout-master-set>
        <fo:page-sequence-master master-name="A4-portrait">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference master-reference="A4-portrait-subsequent" blank-or-not-blank="not-blank" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
        <fo:simple-page-master master-name="A4-portrait-subsequent" margin="2cm">                               >
          <fo:region-body region-name="xsl-region-body" margin-bottom=".5cm" margin-top="0cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>



      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="feat">
    <fo:block border="solid">
        <xsl:value-of select="@Title"/>
        <xsl:apply-templates />
    </fo:block>
  </xsl:template>

  <xsl:template match="Level">
    <fo:block>
        <xsl:value-of select="name()"/>
        <xsl:value-of select="': '"/>
        <fo:inline font-weight="bold">
          <xsl:value-of select="."/>
        </fo:inline >
    </fo:block>
  </xsl:template>

</xsl:stylesheet>
