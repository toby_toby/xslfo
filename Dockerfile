FROM openjdk:11.0.5-jre-stretch

# Install fop
WORKDIR /usr
RUN mkdir /usr/fop && wget https://www-eu.apache.org/dist/xmlgraphics/fop/binaries/fop-2.7-bin.tar.gz -O - | tar -xz -C /usr --strip-components=1


ENV PATH=$PATH:/usr/fop

RUN mkdir /usr/templating
WORKDIR /usr/templating

CMD fop -xml /usr/templating/input/$XMLFILE \
        -xsl /usr/templating/stylesheets/$STYLESHEET \
        -pdf /usr/templating/output/$OUTFILE
 