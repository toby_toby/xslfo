# export XMLFILE=$1
export XMLFILE=dnd.xml


echo Starting conversion

docker run \
    -v `pwd`/input:/usr/templating/input \
    -v `pwd`/stylesheets:/usr/templating/stylesheets \
    -v `pwd`/output:/usr/templating/output \
    -e "XMLFILE=$XMLFILE" \
    -e "STYLESHEET=project.xsl" \
    -e "OUTFILE=projects.pdf" \
    --name templating \
    --rm \
    ramzi/templating:v1

